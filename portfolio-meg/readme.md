# Analysis script for portfolio: MEG, study group 1

This folder has the analysis scripts in ipython notebooks, and two python files with helper functions:
- lau.py has the functions provided by Lau Møller Andersen for preprocessing
- custom_preprocessing.py has some similar helper functions we defined ourselves.

