# plot feature importance 
import lau # used to be prepare_classification.py by Lau
import custom_preprocessing

import mne
from os.path import join
import matplotlib.pyplot as plt
import itertools
import numpy as np
import getpass
from scipy import stats

from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.inspection import permutation_importance
from sklearn.model_selection import GridSearchCV

if getpass.getuser() == "maltelau":
    workdir = '/home/maltelau/cogsci/neuro-23/portfolio-meg'
    meg="MEG_data"
    freesurfer="freesurfer"
else:
    workdir = '/work'
    meg="834761"
    freesurfer="835482"

subject = '0108' # our subject
session = '20230928_000000' # our subject's meg session

meg_path = join(workdir, meg, subject, session, "MEG")
meg_block1 =  join(meg_path, "001.self_block1/files")

bem_path = join(workdir, freesurfer, subject, "bem")
subjects_dir = join(workdir, freesurfer)

raw_name = 'self_block1.fif'
fwd_name = 'self_block1-oct-6-src-5120-fwd.fif'

raw_path = join(workdir, meg)
freesurfer_path = join(workdir, freesurfer)

allsubjects = [
    ['0108', '20230928_000000'],
    ['0109', '20230926_000000'],
    ['0110', '20230926_000000'],
    ['0111', '20230926_000000'],
    ['0112', '20230927_000000'],
    ['0113', '20230927_000000'],
    ['0114', '20230927_000000'],
    ['0115', '20230928_000000']
]

#%%time
models = [LogisticRegression(penalty='elasticnet', C=0.01, solver='saga',l1_ratio=0.01)]

X_source, y = custom_preprocessing.preprocess_all_subjects_source(allsubjects, raw_path, freesurfer_path, decim=10, tmax=1.0)
pos_and_neg_y = custom_preprocessing.collapse_condition(y, "pos_and_neg")
posneg_allsubj = list()

for i, model in enumerate(models):
    print("Starting model", model)
    posneg_allsubj.append(custom_preprocessing.classify_across_time(X_source, pos_and_neg_y, [1, 2], model))

results_posneg = list()
for i, (subject, session) in enumerate(allsubjects[0:1]):
    break
    print("Starting subject", subject)
    X_source, y = custom_preprocessing.preprocess_source_space_data(subject, session,
         raw_path, freesurfer_path, None, to_fsaverage=False,
         decim=10, tmax=1.0) ##CHANGE TO YOUR PATHS # don't go above decim=10
    #X_sensor, y = lau.get_X_and_y(epochs_list)
    X_source, y = custom_preprocessing.filter_wrong_button_presses(X_source, y)
    custom_preprocessing.count_missing_button_presses(y)

    pos_and_neg_y = custom_preprocessing.collapse_condition(y, "pos_and_neg")

    accuracies = list()
    for j, model in enumerate(models):
        print("Starting model", model)
        accuracies = custom_preprocessing.classify_across_time(X_source, pos_and_neg_y, [1,2], model)
    results_posneg.append(accuracies)
    #X_sensor = custom_preprocessing.collapse_time(X_sensor)
    #print(X_sensor.shape)
    #X_sensor = custom_preprocessing.add_block(X_sensor, i)
    #print(X_sensor.shape)
    #times = epochs_list[0].times
    #pos_and_neg_y = gruppe1.collapse_condition()
    #results.posneg.append(gruppe1.classify_across_time(X_sensor, pos_and_neg_y, [1, 2], logr))

_, _, _, feature_importance, model, X_stc, y = posneg_allsubj[0]

epochs_list = custom_preprocessing.preprocess_sensor_space_data('0108', '20230928_000000',
         raw_path=raw_path,  
         tmin=-.2, tmax = 1, baseline = (None,0),
         decim=10) ##CHANGE TO YOUR PATHS # don't go above decim=10

#evoked = epochs_list[0].average()
#evoked.data = results_posneg[0][0][3].reshape(evoked.shape)

data_list = list()
# print(X.shape)
recording_names = ['001.self_block1',  '002.other_block1',
                       '003.self_block2',  '004.other_block2',
                       '005.self_block3',  '006.other_block3']
source_channels = 1280
lambda2=1
method='MNE'
pick_ori='normal'
for epochs_index, epochs in enumerate(epochs_list[0:1]): ## get X

    fwd_fname = recording_names[epochs_index][4:] + '-oct-3-src-' + \
            f'{source_channels}-fwd.fif'
    fwd = mne.read_forward_solution(join(subjects_dir,
                                             subject, 'bem', fwd_fname))
    noise_cov = mne.compute_covariance(epochs, tmax=0.000)
    inv = mne.minimum_norm.make_inverse_operator(epochs.info,
                                                     fwd, noise_cov)


    stcs = mne.minimum_norm.apply_inverse_epochs(epochs, inv, lambda2,
                                                     method, label=None,
                                                     pick_ori=pick_ori, return_generator = True)

       
    data_list = itertools.chain(data_list, stcs)
res = next(data_list)
res.data = feature_importance.importances_mean.reshape(res.data.shape)
res.plot(
    subjects_dir=freesurfer_path,
    hemi="both",
    initial_time=0.1)