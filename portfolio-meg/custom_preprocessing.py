#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

custom_preprocessing.py
helper functions for MEG analysis

@author: Malte Lau Petersen
"""

import lau

import mne
from os.path import join
import matplotlib.pyplot as plt
import numpy as np
import itertools
import multiprocessing
import functools
import random

from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import cross_val_score, permutation_test_score, StratifiedKFold
from sklearn.inspection import permutation_importance


## penalty='none', C=1.0

#logr = LogisticRegression(penalty=penalty, C=C, solver='newton-cg')
#
#
def collapse_time(X):
    return X.reshape((X.shape[0], -1))


def add_block(X, block_id):

    blocks = [
        1,1,2,2,3,3
    ]

    return np.stack((X, np.repeat(blocks[block_id], X.shape[0])), axis = 1)



def classify_across_time(X, y, triggers, model):
    """
    Classify across the entire epoch to get the best possible signal.
    Adapted from lau.simple_classification
    """

    # reshape X to include all (sensor, timepoint) as features in a 2-dimensional matrix
    X = X.reshape((X.shape[0],-1))

    # which rows have data for the conditions we're comparing?
    #indices = lau.get_indices(y, triggers)
    indices = equalize_number_of_indices(y, triggers)
    
    X = X[indices,:]
    y = y[indices]

    sc = StandardScaler() # especially necessary for sensor space as
                          ## magnetometers
                          # and gradiometers are on different scales
                          ## (T and T/m)
    cv = StratifiedKFold()
    
    #n_permutations=100

    sc.fit(X)
    X_std = sc.transform(X)
    #scores = cross_val_score(model, X_std, y, cv=cv)
    #scores, permutation_score, pvalue = permutation_test_score(model, X_std, y, cv=cv)
    print("Pre-fitting model")
    fit = model.fit(X_std, y)
    print("Calculating feature importance")
    feature_importance = permutation_importance(fit, X_std, y)
    
    #mean_score = np.mean(scores)

    return 0,0,0, feature_importance, model, X_std, y
    #return scores, permutation_score, pvalue, feature_importance, model, X_std, y



def equalize_number_of_indices(y, indices):  # write this yourself
    # downsample each category down to the smallest one to ensure equal category sizes
    idx = [lau.get_indices(y, [x]) for x in indices]
    idx_lengths = [len(x) for x in idx]
    min_length = min(idx_lengths)
    for i, category in enumerate(idx):
        num_dropped = idx_lengths[i] - min_length
        if num_dropped > 0:
            print(f"Dropping {num_dropped} triggers '{indices[i]}' to equalize between {indices}. Minimum length {min_length}")
    downsampled_idx = [random.choices(x, k=min_length) for x in idx]
    #print(downsampled_idx)
    return sorted(itertools.chain(*downsampled_idx))


def filter_time(X, y, time_indices, tmin = None, tmax = None):
    if tmin is None and tmax is None:
        raise Exception('Specify at least one of tmin or tmax')

    elif tmin is None:
        tmin = min(time_indices)
    elif tmax is None:
        tmax = max(time_indices)


    idx = np.logical_and(time_indices >= tmin, time_indices <= tmax)

    return X[:,:,idx], y, time_indices[idx]

def preprocess_sensor_space_data(
    subject,
    date,
    raw_path,
    h_freq=40,
    tmin=-0.200,
    tmax=1.000,
    baseline=(None, 0),
    reject=dict(mag=4e-12, grad=4000e-13),#, eog=250e-6),
    decim=4,
):
    """
    Preprocess all 6 blocks for 1 participant.
    """
    recording_names = [
        "001.self_block1",
        "002.other_block1",
        "003.self_block2",
        "004.other_block2",
        "005.self_block3",
        "006.other_block3",
    ]
    # reject = dict(mag=4e-12, grad=4000e-13, eog=250e-6) # T, T/m, V
    epochs_list = list()

    for recording_index, recording_name in enumerate(recording_names):
        # for each block ...
        fif_fname = recording_name[4:]
        full_path = join(
            raw_path, subject, date, "MEG", recording_name, "files", fif_fname + ".fif"
        )
        print(full_path)
        # 1. read raw data
        raw = mne.io.read_raw(full_path, preload=True)
        raw.info['bads'].extend(['MEG0422', 'MEG1312']) # known bad channels to reject
        # 2. do low-pass filtering
        raw.filter(l_freq=None, h_freq=h_freq, n_jobs=multiprocessing.cpu_count())

        # 3. find events
        events = mne.find_events(raw, min_duration=0.002)
        #print(events)
        # 201 and 202 are actual button presses. 23 is the start of the button press trial
        #button_responses = dict(middle_wrong_button = 202, middle_wrong_time = 103)
        #if "self" in recording_name:
        #    event_id = dict(self_positive=11, self_negative=12, button_press=23, **button_responses)
        #elif "other" in recording_name:
        #    event_id = dict(other_positive=21, other_negative=22, button_press=23, **button_responses)
        #else:
        #    raise NameError("Event codes are not coded for file")
        # 4. do epoching
        epochs = mne.Epochs(
            raw, events, None, tmin, tmax, baseline, preload=True, decim=decim,
            reject = reject
        )
        epochs.pick_types(meg=True)

        epochs_list.append(epochs)

        if recording_index == 0:
            X = epochs.get_data()
            y = epochs.events[:, 2]
        else:
            X = np.concatenate((X, epochs.get_data()), axis=0)
            y = np.concatenate((y, epochs.events[:, 2]))

    return epochs_list

def filter_wrong_button_presses(X, y, id_to_filter_out = [103, 203]):
    """
    Filter out trials where our participant pressed a button and weren't
    instructed to, as the motor cortex activation would produce a lot of noise.
    """

    filter_idx, = np.where(functools.reduce(np.logical_or, [y == code for code in id_to_filter_out]))
    #print(filter_idx)
    print(f"Found:: {len(filter_idx)} false alarm trials.")
    if not len(filter_idx) > 0:
        return X, y
    filter_prev = [x - 1 for x in filter_idx]
    #print(filter_prev)
    filter_total = np.concatenate((filter_idx, filter_prev))
    #print(filter_total)
    return np.delete(X, filter_total, axis=0), np.delete(y, filter_total)

    #print(idx)
    #
    #
def count_missing_button_presses(y):
    button_prompt, = np.where(y == 23)
    button_press, = np.where(np.logical_or(y == 202, y == 201))

    num_missed_prompts = len(np.where([y not in button_press for y in button_prompt+1])[0])
    print(f"Found:: {num_missed_prompts} / {len(button_prompt)} missed button presses.")
          #print(button_prompt+1, button_press)
    #print(button_prompt[[y not in button_press for y in button_prompt+1]])
    #print(button_prompt[[y in button_press for y in button_prompt+1]])

def preprocess_all_subjects_sensor(subjects, raw_path, subjects_dir, h_freq=40,
                                   tmin=-0.2, tmax=1., baseline=(None, 0), reject=None, decim=4):
    # X = list()
    # y = list()
    for i, (subject, session) in enumerate(subjects):
        epochs_list = preprocess_sensor_space_data(subject, session, raw_path, h_freq,
                                                       tmin, tmax, baseline, reject, decim)
        this_X, this_y = lau.get_X_and_y(epochs_list)
        # X = np.concatenate((X, this_X))
        # y = np.concatenate((y, this_y))

        if i == 0:
            X = this_X
            y = this_y
        else:
            X = np.concatenate((X, this_X), axis=0)
            y = np.concatenate((y, this_y))
        del this_X, this_y

    X, y = filter_wrong_button_presses(X, y)
    count_missing_button_presses(y)
    return X, y



def preprocess_all_subjects_source(subjects, raw_path, subjects_dir,
                                   method='MNE', lambda2=1, pick_ori='normal',
                                   label=None, decim=4,
                                   source_channels = 1280, tmin=-0.2, tmax=1.0):
    for i, (subject, session) in enumerate(subjects):
        this_X, this_y = preprocess_source_space_data(subject, session, raw_path, subjects_dir,
                                                      None, method=method, lambda2=lambda2, pick_ori=pick_ori,
                                                      label=label, decim=decim, return_generator=False,
                                                      source_channels=source_channels, tmin=tmin, tmax=tmax)

        if i == 0:
            X = this_X
            y = this_y
        else:
            X = np.concatenate((X, this_X), axis=0)
            y = np.concatenate((y, this_y))

        del this_X, this_y
        
    X, y = filter_wrong_button_presses(X, y)
    count_missing_button_presses(y)
    return X, y


def preprocess_source_space_data(subject, date, raw_path, subjects_dir,
                                 epochs_list, to_fsaverage = True,
                                 method='MNE', lambda2=1, pick_ori='normal',
                                 label=None, decim=4, return_generator = False,
                                 source_channels = 1280, tmin=-0.2, tmax=1.0):
    ## adapted from lau's preprocess_source_space_data
    ## added morphing to the template space (fsaverage)

    if epochs_list is None:
        epochs_list = preprocess_sensor_space_data(subject, date, raw_path, decim=decim, tmin=tmin, tmax=tmax)
        #y = np.zeros(len(epochs_list))
    y = np.concatenate([e.events[:,2] for e in epochs_list])

    # for epochs in epochs_list: # get y
    #   y = np.concatenate((y, epochs.events[:, 2]))

    if label is not None:
        if to_fsaverage:
            label_path = join(subjects_dir, 'fsaverage', 'label', label)
        else:
            label_path = join(subjects_dir, subject, 'label', label)
        label = mne.read_label(label_path)

    recording_names = ['001.self_block1',  '002.other_block1',
                       '003.self_block2',  '004.other_block2',
                       '005.self_block3',  '006.other_block3']

        #shape_1epoch = epochs_list[0].get_data().shape
    data_list = list()
    # print(X.shape)
    for epochs_index, epochs in enumerate(epochs_list): ## get X

        fwd_fname = recording_names[epochs_index][4:] + '-oct-3-src-' + \
            f'{source_channels}-fwd.fif'
        fwd = mne.read_forward_solution(join(subjects_dir,
                                             subject, 'bem', fwd_fname))
        noise_cov = mne.compute_covariance(epochs, tmax=0.000)
        inv = mne.minimum_norm.make_inverse_operator(epochs.info,
                                                     fwd, noise_cov)


        stcs = mne.minimum_norm.apply_inverse_epochs(epochs, inv, lambda2,
                                                     method, label=None,
                                                     pick_ori=pick_ori, return_generator = True)

        # malte added:
        # - more memory efficient with python generators
        # - morph to fsaverage

        data_list = itertools.chain(data_list, stcs)
        del fwd, inv, noise_cov, stcs#, this_data, this_X

    if to_fsaverage:
        morph = mne.read_source_morph(join(subjects_dir, subject, 'bem', f"{subject}-oct-3-src-morph.h5"))
        data_list = (morph.apply(stc) for stc in data_list)

    if label:
        data_list = (stc.in_label(label) for stc in data_list)


    if return_generator:
        X = (stc.data for stc in data_list)
    else:
        X = np.stack([stc.data for stc in data_list])
        
    return X, y

def collapse_condition(y, condition):
    positive_y = lau.collapse_events(y, 1, [11, 21])
    negative_y = lau.collapse_events(y, 2, [12, 22])
    self_y =     lau.collapse_events(y, 3, [11, 12])
    other_y =    lau.collapse_events(y, 4, [21, 22])

    if condition == "pos_and_neg":
        return lau.collapse_events(positive_y, 2, [12, 22])
    elif condition == "self_and_other":
        return  lau.collapse_events(self_y, 4, [21, 22])

